<?php
/**
 * Ziki's API integration in Drupal : display your Ziki's profile in a block.
 * @author Arnaud 'Narno' Ligny <arnaud.ligny@narno.com>
 * @copyright Narno.com
 */

/**
 * Implementation of hook_block().
 */
function ziki_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks[0]['info'] = t('Ziki\'s profile');
      return $blocks;
    case 'configure':
      switch($delta) {
        case 0:
          $form['ziki_profile_username'] = array(
            '#type' => 'textfield',
            '#title' => t('Username'),
            '#default_value' => variable_get('ziki_profile_username', NULL),
            '#size' => 30,
            '#maxlength' => 255,
            '#description' => t('Enter your Ziki\'s username. Example "<em>narno</em>"')
          );
          $form['display_photo'] = array(
            '#type' => 'checkbox',
            '#title' => t('Display photo'),
            '#default_value' => variable_get('ziki_block_' . $delta . '_display_photo', TRUE),
            '#description' => t('Show your photo in this block.')
          );
          $form['display_nickname'] = array(
            '#type' => 'checkbox',
            '#title' => t('Display nickname'),
            '#default_value' => variable_get('ziki_block_' . $delta . '_display_nickname', FALSE),
            '#description' => t('Show your nickname in this block.')
          );
          $form['display_baseline'] = array(
            '#type' => 'checkbox',
            '#title' => t('Display baseline'),
            '#default_value' => variable_get('ziki_block_' . $delta . '_display_baseline', TRUE),
            '#description' => t('Show your baseline in this block.')
          );
          $form['display_website'] = array(
            '#type' => 'checkbox',
            '#title' => t('Display website'),
            '#default_value' => variable_get('ziki_block_' . $delta . '_display_website', TRUE),
            '#description' => t('Show your website in this block.')
          );

          return $form;
        break;
      }
    case 'save':
      variable_set('ziki_profile_username', $edit['ziki_profile_username']);
      variable_set('ziki_block_' . $delta . '_display_photo', $edit['display_photo']);
      variable_set('ziki_block_' . $delta . '_display_nickname', $edit['display_nickname']);
      variable_set('ziki_block_' . $delta . '_display_baseline', $edit['display_baseline']);
      variable_set('ziki_block_' . $delta . '_display_website', $edit['display_website']);
    break;
    case 'view':
      switch($delta) {
        case 0:
          $block['subject'] = t('Ziki\'s profile');
          $block['content'] = theme('ziki_block', $delta);
          $block['content'] .= theme('ziki_block_more', variable_get('ziki_profile_username', NULL));
        break;
      }
      return $block;
  }
}

/**
 * theme function for the Ziki block contents
 */
function theme_ziki_block($block) {
  // add css file
  drupal_add_css(drupal_get_path('module', 'ziki') . '/block/ziki_block.css');

  switch ($block) {
    case 0:
      $username = variable_get('ziki_profile_username', 'narno');

      $use_cache = variable_get('ziki_cache_people', FALSE);
      if ($use_cache) {
        $cache_people_id = 'ziki:people_' . $username;
        if ($cached_people = cache_get($cache_people_id)) {
          $people = unserialize($cached_people->data);
        }
      }
      if (!$people) {
        $people = ziki_get_people($username);
      }
      if ($use_cache && $people) {
        cache_clear_all('ziki:people', 'cache', TRUE);
        cache_set($cache_people_id, 'cache', serialize($people), CACHE_TEMPORARY);
      }

      if ($people) {
      	$url       = $people['url'];
      	$firstname = $people['firstname'];
      	$lastname  = $people['lastname'];
        $nickname  = $people['nickname'];
      	$baseline  = $people['baseline'];
      	//$gender    = $people['gender'];
      	//$age       = $people['age'];
      	//$city      = $people['city'];
      	//$country   = $people['country'];
      	$website   = $people['website'];
        // output
        if (variable_get('ziki_block_' . $block . '_display_photo', TRUE) == TRUE) {
          // cached photo ?
          if (variable_get('ziki_cache_people_photo', FALSE) && (variable_get('file_downloads', FILE_DOWNLOADS_PUBLIC) == FILE_DOWNLOADS_PUBLIC)) {
            $photo_cached = ziki_cache_people_photo($username);
            if ($photo_cached) {
              $photo = base_path() . $photo_cached;
            }
          } else {
            //$photo = $profile['photo'];
            $photo  = sprintf(ZIKI_PROFILE_PHOTO_URL, $username);
          }
          $photo = "<img src=\"$photo\" alt=\"$firstname $lastname\" />";
          $output = "\n" . l($photo, ziki_url($username), array(), NULL, NULL, TRUE, TRUE) . "\n";
        }
        $output .= "<ul>\n";
        $output .= "<li><strong>" . l("$firstname $lastname", ziki_url($username)) . "</strong></li>\n";
        if (variable_get('ziki_block_' . $block . '_display_nickname', FALSE) == TRUE) {
          $output .= "<li><strong><em>" . l($nickname, ziki_url($username)) . "</em></strong></a></li>\n";
        }
        if (variable_get('ziki_block_' . $block . '_display_baseline', TRUE) == TRUE) {
          $output .= "<li><em>$baseline</em></li>\n";
        }
        if (variable_get('ziki_block_' . $block . '_display_website', TRUE) == TRUE) {
          $output .= "<li>" . l($website, $website) . "</li>\n";
        }
        $output .= "</ul>\n";
      } else {
        $output = t('no data');
      }
      return $output;
  }
}

/**
 * theme function to provide a more link
 */
function theme_ziki_block_more($username) {
  return "<div class='more-link'>" . l(t('more'), ziki_url($username)) . "</div>\n";
}

function ziki_url($username) {
  if (module_exists('ziki_search')) {
    $link = sprintf(ZIKI_PROFILE_URL_DRUPAL, $username);
  } else {
    $link = sprintf(variable_get('ziki_profile_url', ZIKI_PROFILE_URL), $username);
  }
  return $link;
}

/**
 * download and cache the photo
 * @param $username
 * @return string the local path to the photo
 */
function ziki_cache_people_photo($username) {
  $location = sprintf(ZIKI_PROFILE_PHOTO_URL, $username);
  $directory = file_directory_path() . '/ziki';
  $name = $username . '-' . basename($location) . '.png';
  $file_destination = $directory . '/' . $name;
  if (!file_exists($file_destination)) {
    $result = drupal_http_request($location);
    if ($result->code == 200) {
      if (file_check_directory($directory, FILE_CREATE_DIRECTORY)) {
        return file_save_data($result->data, $directory . '/' . $name, FILE_EXISTS_REPLACE);
      }
    }
  } else {
    return $file_destination;
  }
}
?>
