<?php

/**
 * return an array who contain information about a person
 * @param string username
 * @return array
 */
function ziki_get_people($username) {
  $people = array();
  $xml = @simplexml_load_file(sprintf(variable_get('ziki_api_xml', ZIKI_API_XML), $username));
  if ($xml) {
    $people = array(
      'url'		    => (string)$xml->ziki->url,
      //'photo'		  => (string)$xml->ziki->photos->thumb, // thumb, small, medium
      'firstname' => trim($xml->ziki->first_name),
      'lastname'	=> trim($xml->ziki->last_name),
      'nickname'	=> trim($xml->ziki->nickname),
      'baseline'	=> trim($xml->ziki->baseline),
      'gender'		=> ($xml->ziki->gender == 'male') ? t('Male') : t('Female'),
      'age'		    => t('@age years old', array('@age' => ziki_age($xml->ziki->birthday))),
      'city'		  => (string)$xml->ziki->city,
      'country'	  => (string)$xml->ziki->country,
      'website'	  => (string)$xml->ziki->website
    );
  }
  return $people;
}

/**
 * return age from a birthday date
 * @param string birthday date as YYYY-MM-DD
 * @return integer age
 */
function ziki_age($naiss)  {
	list($annee, $mois, $jour) = split('[-.]', $naiss);
	$today['mois'] = date('n');
	$today['jour'] = date('j');
	$today['annee'] = date('Y');
	$annees = $today['annee'] - $annee;
	if ($today['mois'] <= $mois) {
		if ($mois == $today['mois']) {
			if ($jour > $today['jour'])
				$annees--;
		}
		else
			$annees--;
	}
	return $annees;
}
?>
